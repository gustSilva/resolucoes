package testes;

import exalgoritmos2.NumerosPerfeitos;
import static org.junit.Assert.*;
import org.junit.Test;

public class TesteNumeroPerfeito {

	@Test
	public void testPackageTrueValues() {
		assertTrue(NumerosPerfeitos.NumeroEstranho(1));
		assertTrue(NumerosPerfeitos.NumeroPerfeito(28));
	}

	@Test
	public void testPackageFalseValues() {
		assertFalse(NumerosPerfeitos.NumeroEstranho(22));
		assertFalse(NumerosPerfeitos.NumeroPerfeito(5));
		assertFalse(NumerosPerfeitos.NumeroPerfeito(7));
	}

}
