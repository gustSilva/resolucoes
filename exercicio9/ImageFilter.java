package filterImage;

public class ImageFilter {
	public static int[][] imageFilterByLayer(int[][] m, int layer) {
		int[][] copyOfNewMatrix = copyMatrix(m);
		for (int i = 1; i < copyOfNewMatrix.length - 1; i++) {
			for (int j = 1; j < copyOfNewMatrix[i].length - 1; j++) {
				copyOfNewMatrix[i][j] = medianCalculatorByLayer(m, i, j, 9, layer);
			}
		}
		return copyOfNewMatrix;
	}

	public static int[][] imageFilter9x9(int[][] m) {
		int[][] copyOfNewMatrix = copyMatrix(m);
		for (int i = 1; i < copyOfNewMatrix.length - 1; i++) {
			for (int j = 1; j < copyOfNewMatrix[i].length - 1; j++) {
				copyOfNewMatrix[i][j] = medianCalculator(m, i, j, 9);
			}
		}
		return copyOfNewMatrix;
	}

	public static int[][] imageFilter8x8(int[][] m) {
		int[][] copyOfNewMatrix = copyMatrix(m);
		for (int i = 1; i < copyOfNewMatrix.length - 1; i++) {
			for (int j = 1; j < copyOfNewMatrix[i].length - 1; j++) {
				copyOfNewMatrix[i][j] = medianCalculator(m, i, j, 8);
			}
		}
		return copyOfNewMatrix;
	}

	public static int[][] imageFilter4x4(int[][] m) {
		int[][] copyOfNewMatrix = copyMatrix(m);
		for (int i = 1; i < copyOfNewMatrix.length - 1; i++) {
			for (int j = 1; j < copyOfNewMatrix[i].length - 1; j++) {
				copyOfNewMatrix[i][j] = medianCalculator(m, i, j, 4);
			}
		}
		return copyOfNewMatrix;
	}

	public static int medianCalculatorByLayer(int[][] m, int i, int j, int multi, int layer) {
		int median = 0;
		try {
			if (multi == 4) {
				median = (m[i][j - 1] + m[i][j + 1] + m[i - 1][j] +
						m[i + 1][j] + m[i-layer][j-layer - 1] +
						m[i+layer][j + 1] + m[i - 1][j+layer] +
						m[i + 1][j+layer]) / 4;
			} else if (multi == 8) {
				median = (m[i][j - 1] + m[i - 1][j - 1] + m[i - 1][j] +
						m[i + 1][j] + m[i + 1][j + 1] + m[i][j + 1] +
						m[i - layer][j - layer - 1] + m[i - layer - 1][j - layer - 1]
								+ m[i - layer - 1][j] +
								m[i + layer + 1][j] + m[i + layer + 1][j + layer + 1]
								+ m[i + layer][j + layer + 1]) / 8;
			} else if (multi == 9) {
				median = (m[i][j - 1] + m[i - 1][j - 1] + m[i - 1][j] +
						m[i][j] + m[i + 1][j] + m[i + 1][j + 1] + m[i][j + 1] +
						m[i - layer][j - 1] + m[i - layer - 1][j - layer - 1] +
						m[i - layer - 1][j - layer] + m[i - layer][j - layer] +
						m[i + layer + 1][j + layer] + m[i + layer + 1][j + layer + 1]
						+ m[i + layer][j + layer + 1]) / 9;
			}
			return median;
		} catch (Exception e) {
			e.getMessage();
		}
		return 0;
	}

	public static int medianCalculator(int[][] m, int i, int j, int multi) {
		int median = 0;
		if (multi == 4) {
			median = (m[i][j - 1] + m[i][j + 1] + m[i - 1][j] + m[i + 1][j]) / 4;
		} else if (multi == 8) {
			median = (m[i][j - 1] + m[i - 1][j - 1] + m[i - 1][j] + m[i + 1][j] + m[i + 1][j + 1] + m[i][j + 1]) / 8;
		} else if (multi == 9) {
			median = (m[i][j - 1] + m[i - 1][j - 1] + m[i - 1][j] + m[i][j] + m[i + 1][j] + m[i + 1][j + 1] + m[i][j + 1]) / 9;
		}
		return median;
	}

	public static int[][] copyMatrix(int[][] m) {
		int[][] copyOfNewMatrix = new int[m.length][m[0].length];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				copyOfNewMatrix[i][j] = m[i][j];
			}
		}
		return copyOfNewMatrix;
	}

}
