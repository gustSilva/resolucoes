package Tests;

import filterImage.ImageFilter;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertArrayEquals;

public class testsImageFilter {

	@Test
	public void testmatriztrue() {
		int[][] vec = {
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9},
		};
		int[][] result = {
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9},
		};
		assertArrayEquals("com a matriz vec, aplicando o filtro, " +
						"deverá retornar uma matriz igual.",
				ImageFilter.imageFilter4x4(vec), result);
	}

	@Test
	public void testMatriz4x3True() {
		int[][] vec = {
				{1, 2, 3, 4},
				{4, 5, 6, 7},
				{7, 8, 9, 10},
		};
		int[][] result = {
				{1, 2, 3, 4},
				{4, 5, 6, 7},
				{7, 8, 9, 10},
		};
		assertArrayEquals("Com a matriz vec, aplicando o filtro, " +
						"a matriz deverá ser igual a result",
				ImageFilter.imageFilter4x4(vec), result);
	}

	@Test
	public void testMediaPontosMatriz() {
		int[][] m1 = {
				{0, 1, 2, 3},
				{6, 1, 2, 0},
				{3, 2, 1, 0},
				{0, 1, 2, 3}};
		int[][] expResult1 = {
				{0, 1, 2, 3},
				{6, 2, 1, 0},
				{3, 1, 1, 0},
				{0, 1, 2, 3}};
		assertArrayEquals(expResult1, ImageFilter.imageFilter4x4(m1));
	}

	@Test
	public void testMediaPontosMatriz2() {
		int[][] m2 = {
				{10, 15, 20, 30, 50, 78},
				{4, 8, 14, 50, 20, 70},
				{0, 0, 0, 0, 0, 0},
				{10, 15, 10, 15, 20, 20},
				{100, 100, 100, 100, 100, 100},
				{0, 0, 0, 0, 0, 0}};
		int[][] expResult2 = {
				{10, 15, 20, 30, 50, 78},
				{4, 8, 19, 16, 42, 70},
				{0, 5, 6, 16, 10, 0},
				{10, 30, 32, 32, 33, 20},
				{100, 53, 52, 53, 55, 100},
				{0, 0, 0, 0, 0, 0}};
		assertArrayEquals(expResult2, ImageFilter.imageFilter4x4(m2));
	}

	@Test
	public void testMediaMulti8() {
		int[][] m = {
				{0, 1, 0, 1},
				{1, 2, 2, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
		};
		int[][] expected = {
				{0, 1, 0, 1},
				{1, 0, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
		};
		assertArrayEquals(expected, ImageFilter.imageFilter8x8(m));
	}

	@Test
	public void testMediaMulti9() {
		int[][] m = {
				{0, 1, 0, 1},
				{1, 2, 2, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
		};
		int[][] expected = {
				{0, 1, 0, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
		};
		assertArrayEquals(expected, ImageFilter.imageFilter9x9(m));
	}

	@Test
	public void testMediaLayer2() {
		int[][] m = {
				{0, 1, 0, 1, 0},
				{1, 2, 2, 1, 0},
				{1, 1, 1, 1, 0},
				{1, 1, 1, 1, 0},
				{1, 1, 1, 1, 0}
		};
		int[][] expected = {
				{0, 1, 0, 1, 0},
				{1, 2, 2, 1, 0},
				{1, 1, 2, 1, 0},
				{1, 1, 1, 1, 0},
				{1, 1, 1, 1, 0}
		};
		assertArrayEquals(expected, ImageFilter.imageFilterByLayer(m, 2));
	}
}
