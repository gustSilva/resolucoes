package algoritmos;
import java.lang.Exception;

public class VariasFuncoes {
	public static long Factorial(int num) throws Exception {
		if (num < 0) {
			throw new Exception("VariasFuncoes negativo é impossível.");
		} else {
			long factorial = 1;
			while (num >= 1) {
				factorial = factorial * num;
				num--;
			}
			return factorial;
		}
	}
	public static int maiorDeDois(int a, int b) throws Exception {
		if (a > b) {
			return a;
		} else if (a < b) {
			return b;
		} else {
			throw new Exception ("Os números são iguais");
			//System.out.println("Os números são iguais");
		}
	}
	public static int maiorIntervalo(int a, int b, int c) throws Exception {
		if (a > b && a > c) {
			return a;
		} else if (b > a && b > c) {
			return b;
		} else if (c > a && c > b) {
			return b;
		} else {
			throw new Exception("Dígitos suspeitos.");
		}
	}
	/*
	Estas funções só necessitam de inverter a ordem dos comandos
	public static int menor(int a, int b) {
	public static int menor(int a, int b, int c) {
	*/

	public static int Soma(int[] numeros) {
		int Soma = 0;
		for (int i = 0; i < numeros.length; i++) {
			//Soma += Numeros[i];
			Soma = Soma + numeros[i];
		}
		return Soma;
	}
}
