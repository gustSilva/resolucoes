package testes;

import algoritmos.SomaArrays;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class testeSomaArrays {

	@Test
	public void TestaSomaSimplesTrue() {
		int[] v = {1, 2};
		assertEquals("A soma de um array [1, 2] = 3", SomaArrays.Somador(v), 3);
	}

	@Test
	public void TestaSomaNegativosTrue() {
		int[] v = {-1, -2};
		assertEquals("A soma de um array [-1, -2] = -3", SomaArrays.Somador(v), -3);
	}

	@Test
	public void TesteVerificaArrayVazioTrue() {
		int[] v = new int[4];
		assertEquals("Assumimos um vector vazio antes de lhe atribuir valores", SomaArrays.Somador(v), 0);
	}

	@Test
	public void TestArrayIncompletoTrue() {
		int[] v = new int[4];
		v[3] = 3;
		v[1] = 2;
		assertEquals("Soma arrays incompletos", SomaArrays.Somador(v), 5);
	}
}
