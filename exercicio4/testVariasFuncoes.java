package testes;

import algoritmos.VariasFuncoes;
import org.junit.Test;

import static org.junit.Assert.*;

public class testVariasFuncoes {

	@Test
	public void testFactorialTrueTres() throws Exception {
		assertEquals("VariasFuncoes de 3 = 6", VariasFuncoes.Factorial(3), 6);
	}

	@Test
	public void testFatorialTrueZero() throws Exception {
		assertEquals("VariasFuncoes de 0 = 1", VariasFuncoes.Factorial(0), 1);
	}

	@Test
	public void testFatorialTrueCinco() throws Exception {
		assertEquals("VariasFuncoes de 5 é 120", VariasFuncoes.Factorial(5), 120);
	}

	@Test
	public void testFatorialFalse5() throws Exception {
		assertNotEquals("VariasFuncoes de 5 != 25", VariasFuncoes.Factorial(5), 25);
	}

	@Test
	public void testFatorialFalseZero() throws Exception {
		assertNotEquals("VariasFuncoes de 0 != 0", VariasFuncoes.Factorial(0), 0);
	}

	@Test(expected = Exception.class)
	public void testFatorialTrueNegativo() throws Exception {
		assertEquals("VariasFuncoes de negativo é impossível", VariasFuncoes.Factorial(-1), 0);
	}

	@Test(expected = Exception.class)
	public void testFatorialTrueExcepcaoNegativo() throws Exception {
		assertEquals("VariasFuncoes negativo retorna erros", VariasFuncoes.Factorial(-1), 0);
	}

	@Test
	public void testMaiorDeDoisTrueSimples() throws Exception{
		try {
			assertEquals("Maior de 2 e 3 é 3", VariasFuncoes.maiorDeDois(2, 3), 3);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void testMaiorDeDoisTrueException() {
		try {
			assertEquals("Numeros iguais = exception", VariasFuncoes.maiorDeDois(2, 2), 2);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void testMaiorIntervaloSimples() {
		try {
			assertEquals("3, 2, 1 deve devolver 3", VariasFuncoes.maiorIntervalo(3,2,1), 3);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void testMaiorIntervaloExcepcaoSimples() {
		try {
			assertEquals("3, 3, 1 deve devolver excepção", VariasFuncoes.maiorIntervalo(3, 3, 1), 3);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void testSomaSimples() {
		int[] v = {3, 2, 1};
		assertEquals("A soma de 3, 2 e 1 deve ser 6", VariasFuncoes.Soma(v), 6);
	}
}
