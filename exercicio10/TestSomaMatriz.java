package Tests;

import SomaMatrizCentroEExtremos.SomaMatriz;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSomaMatriz {
	@Test
	public void TesteSomaMatriz5x5() {
		int[][] m = {
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
		};
		int result = 5;
		assertEquals("A soma de uma matriz de uns, 5 por 5, dá 5", result,
				SomaMatriz.somadorMatriz(m));
	}

		@Test
	public void TesteSomaMatrizPar() {
		int[][] m = {
				{1, 1, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
				{1, 1, 1, 1},
		};
		int result = 0;
		assertEquals("A soma de uma matriz par tem que retornar 0", result,
				SomaMatriz.somadorMatriz(m));
	}

	@Test
	public void TesteMatriz7x7() {
		int[][] m = {
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
		};
		int result = 5;
		assertEquals("Para uma matriz 7x7, o método deve retornar 5",
				result, SomaMatriz.somadorMatriz(m));
	}

	@Test
	public void TestMatriz1x1() {
		int[][] m = {
				{1},
		};
		int result = 5;
		assertEquals("Para uma matriz 1x1, deve retornar 5", result,
				SomaMatriz.somadorMatriz(m));
	}

	@Test
	public void OcorrenciasLetras() {
		char[][] m = {
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
		};
		int result = 4;
		assertEquals("Somar o número de ocorrências da letra a", result,
				SomaMatriz.ocorrenciasLetra(m, 'a'));
	}

	@Test
	public void OcorrenciasLetrasFalse() {
		char[][] m = {
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
				{'a', 'b', 'c', 'd', 'e'},
		};
		assertEquals("Somar o número de ocorrências da letra a", 0,
				SomaMatriz.ocorrenciasLetra(m, 'g'));
	}

	@Test
	public void Xadrez() {
		char[][] m = {
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
				{'b', 'b', 'v', 'v', 'v', 'v', 'p', 'p'},
		};
		int brancas = 16;
		assertEquals("Somar o número de ocorrências da letra a", brancas,
				SomaMatriz.ocorrenciasLetra(m, 'b'));
	}


}
