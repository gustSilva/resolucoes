package ArraysManipulationsUsingClasses;

import getPonto.Ponto;

import java.util.ArrayList;
import java.util.List;

public class Array {
	int matrix[][];
	int nLines, nColumns;
	
	public Array(int l, int c) {
		nLines = l;
		nColumns = c;
		matrix = new int[l][c];
	}
	
	public Array(int a[][]) {
		matrix = new int[a.length][a[0].length];
		nLines = a.length;
		nColumns = a[0].length;
		for (int i = 0; i < nLines ; i++) {
			for (int j = 0; j < nColumns; j++) {
				matrix[i][j] = a[i][j];
			}
		}
	}
	
	public int getInPonto(Ponto p) {
		return matrix[p.getL()][p.getC()];
	}
	
	public void replacePonto(Ponto p, int num) {
		final int MATRIXMINIMUM = 0; 
		final int MATRIXMAXIMUM = matrix[0].length;
		if ((p.getL() >= MATRIXMINIMUM) && (p.getC() <= MATRIXMAXIMUM)) {
			matrix[p.getL()][p.getC()] = num;
		}
	}
	
	public int[][] getMatrix() {
		return this.matrix;
	}
	
	@Override
	public boolean equals(Object other) {
		boolean res = false;
		if(other instanceof Array) {
			Array m2 = (Array) other;
			if(m2.nLines == this.nLines && m2.nColumns == this.nColumns) {
				res = true;
				for (int i = 0; i < this.nLines && res; i++) {
					for (int j = 0; j < this.nColumns && res; j++) {
						res = m2.matrix[i][j] == this.matrix[i][j];
					}
				}
			}
		}
		return res;
	}
	
	public Array ArraySum(Array b) {
		Array matrixSoma = new Array(matrix.length, matrix[0].length);
		for (int l = 0; l < matrix.length; l++) {
			for (int c = 0; c < matrix[0].length; c++) {
				matrixSoma.matrix[l][c] = matrix[l][c]+ b.matrix[l][c];
			}
		}
		return matrixSoma;
	}
	
	public Array ArrayMutiply(Array b) {
		Array multiplicationArray = new Array(matrix.length, matrix[0].length);
		for (int l = 0; l < matrix.length; l++) {
			for (int c = 0; c < matrix[0].length; c++) {
				multiplicationArray.matrix[l][c] = 
						((matrix[l][c] * b.matrix[c][l]) + 
								(matrix[l+1][c] * b.matrix[l][c+1]));
				l++;
			}
		}
		return multiplicationArray;
	}
	
	public List<Ponto> getPontosIguais(int num) {
		List<Ponto> result = new ArrayList<>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if(matrix[i][j] == num) {
					result.add(new Ponto(i, j));
				}
			}
		}
		return result;
	}
	
	public Array Transpose() {
		Array baseArray = new Array(this.matrix[0].length, this.matrix.length);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				baseArray.matrix[j][i] = this.matrix[i][j];
			}
		}
		return baseArray;
	}
	
	public boolean isTriangular() {
		if(isSquared());
		if(isTriangularSup() || isTriangularDWN());
		return true;
	}
	
	private boolean isTriangularDWN() {
		boolean isTriangularDWN = true;
		for (int i = 0; i < (this.nLines)-1; i++) {
			for (int j = i+1; j < this.nColumns-1; j++) {
				if (this.matrix[j][i] != 0) {
					return false;
				}
			}
		}
		return isTriangularDWN;
	}
	
	private boolean isTriangularSup() {
		boolean isTriangularSUP = true;
		for (int i = 0; i < this.nLines-1; i++) {
			for (int j = i+1; j < this.nColumns-1; j++) {
				if (this.matrix[i][j] != 0) {
					return false;
				}
			}
		}
		return isTriangularSUP;
	}
	
	private boolean isSquared() {
		return (this.nColumns == this.nLines);
	}
	
}
