package IgualdadeVectores;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Vectores {
	public static Boolean Igualdades(int[] a, int[] b) {
		boolean found = false;
		while (found != true) {
			if (a.length != b.length) return false;
			for (int i = 0; i < a.length; i++) {
				if (a[i] != b[i]) {
					found = true;
					return false;
				} else {
					i++;
				}
			}
			found = true;
		}
		return true;
	}

	public static Boolean ArrayIgual(int[] a) {
		boolean found = true;
		for(int i=0, j = a.length; i < (a.length/2) && found; i++, j--) {
			found = a[i]==a[j-1];
		}
		return found;
	}

	public static int EncontraElemento(int[] a, int num) {
		for(int i = 0; i < a.length; i++) {
			if (a[i] == num)
				return i;
		}
		return -1;
	}

}
