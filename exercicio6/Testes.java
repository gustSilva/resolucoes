package Testes;

import IgualdadeVectores.MatrixSym;
import IgualdadeVectores.Vectores;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Testes {
	@Test
	public void ComparacaoVetores() {
		int[] a = new int[]{1, 2, 3, 4};
		int[] b = new int[]{1, 2, 3, 4};
		assertEquals("Dois vetores [1, 2, 3, 4] devem retornar true", Vectores.Igualdades(a, b), true);
	}

	@Test
	public void ComparacaoVetoresDiff() {
		int[] a = new int[]{1, 2, 3};
		int[] b = new int[]{2, 3};
		assertEquals("Vetores de dimensões diferentes devem retornar false", Vectores.Igualdades(a, b), false);
	}

	@Test
	public void ComparacaoDiferentes() {
		int[] a = new int[]{1, 2, 3};
		int[] b = new int[]{2, 3, 1};
		assertEquals("Um vector [1, 2, 3] e [2, 3, 1] deve retonar false", Vectores.Igualdades(a, b), false);
	}

	@Test
	public void NumeroEncontrado(){
		int[] a = new int[]{1, 2, 3, 4};
		int num = 4;
		assertEquals("4 Enconntra-se na posição 3 num vetor [1,2,3,4]", Vectores.EncontraElemento(a, num), 3);
	}

	@Test
	public void NumeroNaoEncontrado(){
		int[] a = new int[]{1, 2, 3, 4};
		int num = 9;
		assertEquals("4 Enconntra-se na posição 3 num vetor [1,2,3,4]", Vectores.EncontraElemento(a, num), -1);
	}

	@Test
	public void EncontrarNumeroVetorVazio(){
		int [] a = new int[]{};
		int num = 1;
		assertEquals("Encontrar 1 num vetor vazio", Vectores.EncontraElemento(a, num), -1);
	}

	@Test
	public void IsMatrixSymmetricFalse() {
		int[][] m = new int[][] {{0,1}, {8, 3}};
		assertEquals("Numa matrix {[0,1], [8, 3]}, return false", MatrixSym.IsSymmetric(m), false);
	}

	@Test
	public void IsMatrixSymmetricTrue() {
		int[][] m = new int[][]{{0,1,3}, {1, 3, 4}, {3, 4, 5}};
		assertEquals("Numa matrix {[0,1], [8, 3]}, return false", MatrixSym.IsSymmetric(m), true);
	}
}
