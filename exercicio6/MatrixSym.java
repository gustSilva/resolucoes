package IgualdadeVectores;

public class MatrixSym {
		public static Boolean IsSymmetric(int[][] m) {
			boolean sym = true;
			for(int i=0; i<m.length-1; i++) {
				for (int j = i+1; j < m.length; j++) {
					if(m[i][j] != m[j][i]){
						sym = false;
						break;
					}
				}
			}
			return sym;
		}
	}
