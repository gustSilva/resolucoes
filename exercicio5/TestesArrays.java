package TestesExerciciosArrays;

import ExerciciosArrays.ManipulacaoArrays;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TestesArrays {
	@Test
	public void TesteNumeroDeElementosTrue() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("Um array [2, 3, 4] = 3", ManipulacaoArrays.NumVector(vec), 3);
	}

	@Test
	public void TestNumeroElementosVazio() {
		int[] vec = new int[]{};
		assertEquals("Um array vazio [] = 0", ManipulacaoArrays.NumVector(vec), 0);
	}

	@Test
	public void TestMaiorElemento() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = 3", ManipulacaoArrays.MaiorVector(vec), 3);
	}

	@Test
	public void TesteMaiorElementoArrayVazio() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = 0", ManipulacaoArrays.MaiorVector(vec), 0);
	}
	@Test
	public void TestMenorElemento() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = 3", ManipulacaoArrays.MenorVector(vec), 1);
	}

	@Test
	public void TesteMenorElementoArrayVazio() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = 0", ManipulacaoArrays.MenorVector(vec), 0);
	}

	@Test
	public void TesteSomatorioNumerosArray() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = 6", ManipulacaoArrays.SomaVector(vec), 6);
	}

		@Test
	public void TesteSomatorioNumerosArrayVazio() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = 0", ManipulacaoArrays.SomaVector(vec), 0);
	}

	@Test
	public void TesteSomatorioNumerosArrayPar() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = 2", ManipulacaoArrays.SomaVectorPares(vec), 2);
	}

	@Test
	public void TesteSomatorioNumerosArrayParVazio() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = 0", ManipulacaoArrays.SomaVectorPares(vec), 0);
	}

	@Test
	public void TesteSomatorioNumerosArrayPar1() {
		int[] vec = new int[]{2, 4, 5};
		assertEquals("De um array [2, 4, 5] = 6", ManipulacaoArrays.SomaVectorPares(vec), 6);
	}
	@Test
	public void TesteSomarioMultiplosNumArrayVazio() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = 0", ManipulacaoArrays.SomaVectorMultiplos(vec, 2), 0);
	}

		@Test
	public void TesteSomatorioMultiplosNumArraySimples() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = 2", ManipulacaoArrays.SomaVectorMultiplos(vec, 2), 2);
	}

	@Test
	public void TesteSomatorioMultiplosNumArrayCompleto() {
		int[] vec = new int[]{5, 15, 30};
		assertEquals("De um array [5, 15, 30] = 50", ManipulacaoArrays.SomaVectorMultiplos(vec, 5), 50);
	}

	@Test
	public void TestIsVecEmptyFalse() {
		int[] vec = new int[]{5, 15, 30};
		assertEquals("De um array [5, 15, 30] = true", ManipulacaoArrays.isVecEmpty(vec), false);
	}

	@Test
	public void TestIsVecEmptyTrue() {
		int[] vec = new int[]{};
		assertEquals("De um array [] = false", ManipulacaoArrays.isVecEmpty(vec), true);
	}

	@Test
	public void TestIsVecOneOnly() {
		int[] vec = new int[]{1};
		assertEquals("De um array [1] = false", ManipulacaoArrays.isVecEmpty(vec), true);
	}

	@Test
	public void TestIsInteiro() {
		double[] vec = new double[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = true", ManipulacaoArrays.isVecInteiro(vec), true);
	}

	@Test
	public void testIsInteiroFalse() {
		double[] vec = new double[]{1, 2.1, 3.1};
		assertEquals("De um array [1, 1, 3] = true", ManipulacaoArrays.isVecInteiro(vec), false);
	}
	@Test
	public void TestIsRepeatedFalse() {
		int[] vec = new int[]{1, 2, 3};
		assertEquals("De um array [1, 2, 3] = false", ManipulacaoArrays.isVecRepeated(vec), false);
	}

	@Test
	public void TestIsRepeatedTrue() {
		int[] vec = new int[]{1, 1, 31};
		assertEquals("De um array [1, 1, 31] = true", ManipulacaoArrays.isVecRepeated(vec), true);
	}

	@Test
	public void TestIsRepeatedTrue2() {
		int[] vec = new int[]{1, 2, 1};
		assertEquals("De um array [1, 2, 1] = true", ManipulacaoArrays.isVecRepeated(vec), true);
	}

	@Test
	public void TestMultiplos3NumArrayTrue() {
		int[] vec = new int []{3, 6, 9, 10};
		List<Integer> result = new ArrayList<>(Arrays.asList(3, 6, 9));

		assertEquals("Num array de [3, 6, 9, 10] = [3, 6, 9]", ManipulacaoArrays.MultiplosTres(vec), result);
	}

	@Test
	public void TestMultiplos3NumArrayVazio() {
		int[] vec = new int []{};
		assertEquals("Num array de [] = 0", ManipulacaoArrays.MultiplosTres(vec), Arrays.asList());
	}

	@Test
	public void TestMultiplosNumArray() {
		int[] vec = new int []{3, 6, 9, 10};
		List<Integer> result = new ArrayList<Integer>(Arrays.asList(3, 6, 9));
		assertEquals("Num array de [3, 6, 9, 10] = [3, 6, 9]", ManipulacaoArrays.MultiplosN(vec, 3), result);
	}

	@Test
	public void TestMultiplosNumArrayVazio() {
		int[] vec = new int []{};
		assertEquals("Num array de [] = []", ManipulacaoArrays.MultiplosN(vec, 3), Arrays.asList());
	}

	@Test
	public void TestMuiltiplos2NumIntTrue() {
		int[] vec = new int []{15, 30, 45, 60, 70};
		List<Integer> result = new ArrayList<Integer>(Arrays.asList(15, 30, 45, 60));
		assertEquals("Num array de [15, 30, 45, 60, 70] = [15, 30, 45, 60]", ManipulacaoArrays.Multiplos2N(vec, 5, 15), result);
	}

	@Test
	public void TestMultiplos2NumIntArrayVazio() {
		int[] vec = new int []{};
		assertEquals("Num array de [3, 6, 9, 10] = [3, 6, 9]", ManipulacaoArrays.Multiplos2N(vec, 5, 15), Arrays.asList());
	}

	@Test
	public void TestOrdenadoArray() {
		int[] vec = new int[]{1, 2, 3, 4};
		List<Integer> result = new ArrayList<>(Arrays.asList(4, 3, 2, 1));
		assertEquals("Num array [4, 3, 2, 1] = [1, 2, 3, 4]", ManipulacaoArrays.OrdenaArray(vec), result);
	}

	@Test
	public void TestOrdenadoArrayOutro() {
		int[] vec = new int[]{2, 1, 10, 4};
		List<Integer> result = new ArrayList<>(Arrays.asList(10, 4, 2, 1));
		assertEquals("Num array [2, 1, 10, 4] = [1, 2, 4, 10]", ManipulacaoArrays.OrdenaArray(vec), result);
	}

		@Test
	public void TestOrdenadoArrayVazio() {
		int[] vec = new int[]{};
		List<Integer> result = new ArrayList<>(Arrays.asList());
		assertEquals("Num array [] = []", ManipulacaoArrays.OrdenaArray(vec), result);
	}

	@Test
	public void TestOrdenarArrayComMultiplosN() {
		int[] vec = new int[]{44,-9,120,100,-9,66,6,4,18,24,25};
		List<Integer> result = ManipulacaoArrays.OrdenaArrayMultiplosN(vec, 2, 3, 1, 50);
		assertEquals("Encontra multiplos de 2 numeros num intervalo dado",
				ManipulacaoArrays.OrdenaArrayMultiplosN(vec, 2, 3, 5, 24), result);

	}

}

