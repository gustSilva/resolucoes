package ExerciciosArrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class ManipulacaoArrays {
	public static int NumVector(int[] vec) {
		return vec.length;
	}

	public static int MaiorVector(int[] vec) {
		int maior = 0;
		for (int i = 1; i < vec.length; i++) {
			if (vec[i] > maior)
				maior = vec[i];
		}
		return maior;
	}

	public static int MenorVector(int[] vec) {
		try {
			int maior = vec[0];
			for (int i = 0; i < vec.length; i++) {
				if (maior > vec[i]) {
					maior = vec[i];
				}
			}
			return maior;
		} catch (Exception e) {
			return 0;
		}
	}

	public static int SomaVector(int[] vec) {
		int soma = 0;
		if (vec.length == 0) {
			return 0;
		} else {
			for (int i = 0; i < vec.length; i++) {
				soma += vec[i];
			}
		}
		return soma;
	}

	public static int SomaVectorPares(int[] vec) {
		int somaPar = 0;
		if (vec.length == 0) {
			return 0;
		} else {
			for (int i = 0; i < vec.length; i++) {
				if (vec[i] % 2 == 0) {
					somaPar += vec[i];
				}
			}
		}
		return somaPar;
	}

	public static int SomaVectorMultiplos(int[] vec, int m) {
		int somaMultiplos = 0;
		if (vec.length == 0) {
			return 0;
		} else {
			for (int i = 0; i < vec.length; i++) {
				if (vec[i] % m == 0) {
					somaMultiplos += vec[i];
				}
			}
		}
		return somaMultiplos;
	}

	public static boolean isVecEmpty(int[] vec) {
		if (vec.length == 0 || vec.length == 1)
			return true;
			return false;
	}

	public static boolean isVecInteiro(double[] vec) {
		for(int i = 0; i < vec.length; i++) {
			if (vec.length == 0 ) {
				return true;
			} else if (vec[i] % 1 != 0) {
				return false;
			}
		}
		return true;

	}

  	public static boolean isVecRepeated(int[] vec) {
		for (int i = 0; i < vec.length-1; i++) {
			for (int j = i+1; j < vec.length; j++) {
				if (vec[i] == vec[j])
					return true;
			}
		}
	    return false;
    }

	public static List<Integer> MultiplosTres(int[] vec) {
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < vec.length; i++) {
			if (vec[i] % 3 == 0) {
				result.add(vec[i]);
			}
		}
		return result;
	}

	public static List<Integer> MultiplosN(int[] vec, int n) {
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < vec.length; i++) {
			if (vec[i] % n == 0) {
				result.add(vec[i]);
			}
		}
		return result;
	}

	public static List<Integer> Multiplos2N(int[] vec, int n, int m) {
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < vec.length; i++) {
			if (vec[i] % n == 0 && vec[i] % m == 0) {
				result.add(vec[i]);
			}
		}
		return result;
	}

	public static ArrayList<Integer> OrdenaArray(int[] vec) {
		int temp = 0;
		for (int i = 0; i < vec.length-1; i++) {
			temp = vec[i];
			for (int j = i+1; j < vec.length; j++) {
				if (vec[i] < vec[j]) {
					temp = vec[j];
					vec[j] = vec[i];
					vec[i] = temp;
				}
			}
		}
		return new ArrayList<Integer>(){{ for (int k : vec) add(k); }};
	}

	public static ArrayList<Integer> OrdenaArrayMultiplosN(int[] vec, int n, int m, int inf, int sup) {
		ArrayList<Integer> temp = (ArrayList<Integer>) Multiplos2N(vec, n, m);
		Collections.reverse(temp);
		ArrayList<Integer> aux = new ArrayList<>();

		int j = 0;
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i) >= inf && temp.get(i) <= temp.get(i)) {
				aux.add(j, temp.get(i));
				j++;
			}
		}
		return temp;
	}
}
