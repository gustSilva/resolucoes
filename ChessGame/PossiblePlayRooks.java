package PossiblePlays;

public class PossiblePlayRooks {

	/**
	 * Method to check if a Rook move is valid in a chess game.
	 * It uses several secondary methods to perform this check.
	 *
	 * @param tab      Refers to the board
	 * @param posInit  Refers to the initial position requested.
	 * @param posFinal Refers to the final position requested.
	 * @returns true or false.
	 * @author gsilpvat
	 */
	public static boolean isRookPlayAllowed(char[][] tab, int[] posInit,
	                                        int[] posFinal) {
		if (isDirectionAllowedForRooks(posInit, posFinal)) ;
		if (RulesToCheck.isInTab(posInit)) ;
		if (RulesToCheck.isInTab(posFinal)) ;
		if (RulesToCheck.isValidPath(tab, posInit, posFinal)) ;
		return true;
	}


	/**
	 * Method to check if directions given are allowed for Rooks.
	 *
	 * @param posInit  refers to the initial position of the piece.
	 * @param posFinal refers to the final position of the piece.
	 * @returns true if either it is an horizontal or vertical move,
	 * false if oblique.
	 */
	public static boolean isDirectionAllowedForRooks(int[] posInit, int[] posFinal) {
		int[] vec = new int[2];
		for (int i = 0; i < 2; i++) {
			vec = RulesToCheck.moveDirection(posInit, posFinal);
		}
		if (vec[0] == vec[1]) {
			return false;
		}
		return true;
	}
}


