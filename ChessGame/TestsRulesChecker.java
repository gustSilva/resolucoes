package PossiblePlaysTests;

import PossiblePlays.PossiblePlayBishops;
import PossiblePlays.PossiblePlayRooks;
import PossiblePlays.RulesToCheck;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestsRulesChecker {

	@Test
	public void isInTab() {
		int[] pos = {0, 9};
		assertFalse("Checks position [0,9] is out of the board and " +
				"returns false", RulesToCheck.isInTab(pos));
	}


	@Test
	public void cannotTakePlaceOfOtherEqualRook() {
		char[][] m = {
				{'t', 't', 'v', 'v', 'v', 'v', 'P', 'T'},
		};
		int[] posInitial = {0, 1};
		int[] posFinal = {0, 1};
		assertFalse("Shows it would stop if a rook would try moving " +
						"towards the other rook",
				RulesToCheck.canLand(m, posInitial, posFinal));
	}

	@Test
	public void cannotLandInPieceOfSameColor() {
		char[][] m = {
				{'t', 'p', 'v', 'k', 'v', 'v', 'P', 'T'},
		};
		int[] posInitial = {0, 1};
		int[] posFinal = {0, 3};
		assertFalse("Shows it recognizes pieces of same color " +
						"and does not allow the movement.",
				RulesToCheck.canLand(m, posInitial, posFinal));
	}

	@Test
	public void cannotTakeOpponentPlaceIfMateInFron() {
		char[][] m = {
				{'t', 'p', 'v', 'P', 'v', 'v', 'P', 'T'},
		};
		int[] posInitial = {0, 0};
		int[] posFinal = {0, 1};
		assertFalse("Shows it can distinguish " +
				"pieces by color which are represented by Upper and Lower" +
				"case", RulesToCheck.canLand(m, posInitial, posFinal));
	}

	@Test
	public void canTakeOpponentPlace() {
		char[][] m = {
				{'t', 'v', 'v', 'P', 'v', 'v', 'P', 'T'},
		};
		int[] posInitial = {0, 0};
		int[] posFinal = {0, 3};
		assertTrue("Shows it can distinguish" +
				"pieces by color which are represented by Upper and Lower" +
				"case", RulesToCheck.canLand(m, posInitial, posFinal));
	}

	@Test
	public void isDirectionValid() {
		int[] expected = {0, 1};
		int[] posInit = {0, 0};
		int[] posFinal = {0, 4};
		assertArrayEquals("Checks if direction chosen is horizontal" +
						"or vertical",
				expected, RulesToCheck.moveDirection(posInit, posFinal));
	}

	@Test
	public void isPathClearTrue() {
		char[][] m = {
				{'t', 'v', 'v', 'v', 'v', 'v', 'P', 'T'},
		};
		int[] posInitial = {0, 0};
		int[] posFinal = {0, 5};
		assertTrue("Test to confirm it allows a path that is empty",
				RulesToCheck.isValidPath(m, posInitial, posFinal));
	}

	@Test
	public void isPathClearFalse() {
		char[][] m = {
				{'t', 'v', 'v', 'p', 'v', 'v', 'P', 'T'},
		};
		int[] posInit = {0, 0};
		int[] posFinal = {0, 5};
		assertFalse("Test to confirm it does not allow a non-empty" +
				"path", RulesToCheck.isValidPath(m, posInit, posFinal));
	}


	@Test
	public void canLandTestFalse() {
		char[][] m = {
				{'t', 'p', 'v', 'v', 'v', 'v', 'P', 'T'},
		};
		int[] posInit = {0, 0};
		int[] pos = {0, 1};
		assertFalse("Test to confirm it validates the landing " +
				"coordinates properly", RulesToCheck.canLand(m, posInit, pos));
	}

	@Test
	public void canLandTestTrue() {
		char[][] m = {
				{'t', 'v', 'v', 'v', 'v', 'v', 'P', 'T'},
		};
		int[] posInit = {0, 0};
		int[] pos = {0, 2};
		assertTrue("Test to confirm it validates the landing" +
				"coordinates properly", RulesToCheck.canLand(m, posInit, pos));
	}

	@Test
	public void isVerticalMovesAllowed() {
		int[] posInit = {0, 0};
		int[] posFinal = {0, 3};
		assertTrue("Test to confirm it allows vertical moves from " +
						"a rook.",
				PossiblePlayRooks.isDirectionAllowedForRooks(posInit, posFinal));
	}

	@Test
	public void isObliqueMovesNotAllowedTrue() {
		int[] posInit = {0, 0};
		int[] posFinal = {3, 3};
		assertFalse("Test to confirm it will invalidate an Oblique " +
				"move from a rook",
				PossiblePlayRooks.isDirectionAllowedForRooks(posInit, posFinal));
	}

	@Test
	public void isDirectionValidForwDown() {
		int[] expected = {1, 1};
		int[] posInit = {1, 1};
		int[] posFinal = {2, 2};
		assertArrayEquals("Checks it accepts moving forward" +
						"and down.",
				expected, RulesToCheck.moveDirection(posInit, posFinal));
	}

	@Test
	public void isDirectionValidForwUp() {
		int[] expected = {1, -1};
		int[] posInit = {0, 3};
		int[] posFinal = {3, 0};
		assertArrayEquals("Checks it accepts moving forward" +
						"and up.",
				expected, RulesToCheck.moveDirection(posInit, posFinal));
	}

	@Test
	public void IsDirectionValidBackUp() {
		int[] expected = {-1, -1};
		int[] posInit = {3, 1};
		int[] posFinal= {2, 0};
		assertArrayEquals("Checks if it accepts moving backwards" +
						"and upwards",
				expected, RulesToCheck.moveDirection(posInit, posFinal));
	}

	@Test
	public void IsDirectionValidBackDown() {
		int[] expected = {-1, -1};
		int[] posInit = {2, 2};
		int[] posFinal = {1, 1};
		assertArrayEquals("Checks if it accepts moving backwards" +
						"and down",
				expected, RulesToCheck.moveDirection(posInit, posFinal));
	}

	@Test
	public void isHorizontalMovesAllowed() {
		int[] posInit = {0, 0};
		int[] posFinal = {3, 0};
		assertFalse("Test to confirm it denies horizontal moves from " +
						"a bishop.",
				PossiblePlayBishops.isDirectionAllowedForBishops(posInit, posFinal));
	}

	@Test
	public void isObliqueMoveAllowed() {
		int[] posInit = {0, 0};
		int[] posFinal = {3, 3};
		assertTrue("Test to confirm it will validate an oblique " +
						"move from a bishop",
				PossiblePlayBishops.isDirectionAllowedForBishops(posInit, posFinal));
	}

	@Test
	public void isOnlyDiagonal() {
		int[] posInit = {0, 1};
		int[] posFinal = {3, 3};
		assertFalse("Test to confirm it will deny a non-diagonal" +
						"move from a bishop",
				PossiblePlayBishops.isMoveDiagonal(posInit, posFinal));
	}

	@Test
	public void isMoveOnlyDiagonal() {
		int[] posInit = {1, 1};
		int[] posFinal = {3, 3};
		assertTrue("Test to confirm it will deny a non-diagonal" +
						"move from a bishop",
				PossiblePlayBishops.isMoveDiagonal(posInit, posFinal));
	}

}
