package PossiblePlaysTests;

import PossiblePlays.PossiblePlayPawn;
import PossiblePlays.RulesToCheck;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestsPawnMovement {

	@Test
	public void canOnlyMoveOneStepAtATimeFalseCase() {
		int[] posInit = {0, 2};
		int[] posFinal = {0, 4};
		assertFalse("Moving a Pawn cannot be lower than 1 place at" +
						"a time.",
				PossiblePlayPawn.isOnlyOneForward(posInit, posFinal));
	}

	@Test
	public void canOnlyMoveOneStepAtATimeTrueCase() {
		int[] posInit = {0, 2};
		int[] posFinal = {1, 2};
		assertTrue("Moving a Pawn can only be 1 place at a time.",
				PossiblePlayPawn.isOnlyOneForward(posInit, posFinal));
	}

	@Test
	public void MovementIsOnlyForwardFalseCase() {
		int[] posInit = {1, 2};
		int[] posFinal = {0, 2};
		assertFalse("Moving a Pawn backwards is not allowed",
				PossiblePlayPawn.isOnlyOneForward(posInit, posFinal));
	}

	@Test
	public void MovementIsOnlyForwardTrueCase() {
		int[] posInit = {1, 2};
		int[] posFinal = {2, 2};
		assertTrue("Moving a pawn forward is allowed",
				PossiblePlayPawn.isOnlyOneForward(posInit, posFinal));
	}

	@Test
	public void canTakeOpponentPlaceIfDiagonalTrueCase() {
		char[][] m = {
				{'t', 'p', 'v'},
				{'t', 'p', 'P'},
				{'t', 'p', 'v'},
		};
		int[] posInit = {0, 1};
		int[] posFinal = {2, 1};
		assertTrue("A pawn can take place of an oppenent if movement " +
						"is diagonal",
				PossiblePlayPawn.isOpponentDiagonal(m, posInit, posFinal));

	}

	@Test
	public void cannotTakeOpponentPlaceIfInFront() {
		char[][] m = {
				{'t', 'p', 'P'},
				{'t', 'p', 'v'},
				{'t', 'p', 'v'},
		};
		int[] posFinal = {0, 2};
		assertFalse("A pawn cannot take an opponent's place in front" +
						"of him",
				RulesToCheck.isEmpty(m, posFinal));
	}

}
