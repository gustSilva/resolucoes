package PossiblePlays;

public class PossiblePlayQueen {
	/**
	 * Method to check if a Queen move is valid or not.
	 * It checks if the desired move is Diagonal and, if not, uses the Rooks
	 * methods to confirm if it is allowed or not. If it is diagonal, it goes
	 * to the bishop's method, and uses those instead.
	 *
	 * @param tab      Refers to the board.
	 * @param posInit  Refers to the initial position.
	 * @param posFinal Refers to the ending position.
	 * @return true if allowed, false otherwise.
	 */
	private static boolean isPlayQueenAllowed(char[][] tab, int[] posInit,
	                                          int[] posFinal) {
		if (RulesToCheck.isInTab(posInit)) ;
		if (RulesToCheck.isInTab(posFinal)) ;
		if (PossiblePlayBishops.isMoveDiagonal(posInit, posFinal)) {
			if (PossiblePlayBishops.isDirectionAllowedForBishops(posInit,
					posFinal)) ;
		} else {
			PossiblePlayRooks.isDirectionAllowedForRooks(posInit, posFinal);
		}
		if (RulesToCheck.isValidPath(tab, posInit, posFinal)) ;
		return true;
	}
}
