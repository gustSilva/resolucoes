package PossiblePlays;

public class PossiblePlayBishops {

	/**
	 * Method to check whether a Bishop play is allowed or not.
	 * It uses most of the methods composed in the PossiblePlayRooks.java class
	 *
	 * @param tab      Refers to the board.
	 * @param posInit  Refers to the initial position.
	 * @param posFinal Refers to the final position.
	 * @return true or false.
	 * @author gsilvapt
	 */
	private static boolean isPlayBishopAllowed(char[][] tab, int[] posInit,
	                                           int[] posFinal) {
		if (isMoveDiagonal(posInit, posFinal)) ;
		if (isDirectionAllowedForBishops(posInit, posFinal)) ;
		if (RulesToCheck.isInTab(posInit)) ;
		if (RulesToCheck.isInTab(posFinal)) ;
		if (RulesToCheck.isValidPath(tab, posInit, posFinal)) ;
		return true;
	}

	/**
	 * Method to confirm if positions given are oblique directions or not.
	 *
	 * @param posInit  refers to the initial positon
	 * @param posFinal refers to the final position.
	 * @return a vector {1, 1} or {-1, -1} accordingly.
	 */
	public static boolean isDirectionAllowedForBishops(
			int[] posInit, int[] posFinal) {
		int[] vec = new int[2];
		for (int i = 0; i < 2; i++) {
			vec = RulesToCheck.moveDirection(posInit, posFinal);
		}
		if (vec[0] == vec[1]) {
			return true;
		}
		return false;
	}

	/**
	 * Method to check if given coordinates represent diagonal moves or not.
	 *
	 * @param posInit  Refers to the starting position
	 * @param posFinal Refers to the final position.
	 * @return true if movement is diagonal. False otherwise.
	 */
	public static boolean isMoveDiagonal(int[] posInit, int[] posFinal) {
		return Math.abs(posInit[0] - posFinal[0]) ==
				Math.abs(posInit[1] - posFinal[1]);
	}


}