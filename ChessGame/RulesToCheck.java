package PossiblePlays;

public class RulesToCheck {

	/**
	 * Method to check if given coordinates are within the board.
	 *
	 * @param pos Refers to given position.
	 * @returns true if pos is within the range.
	 */
	public static boolean isInTab(int[] pos) {
		for (int i = 0; i < 2; i++) {
			if (pos[i] < 0 || pos[i] > 7) return false;
		}
		return true;
	}

	/**
	 * Method to check that, for all positions in between the initial and final
	 * it canLand() and/or isEmpty.
	 * It uses auxiliary variables to go through the entire loop without
	 * exceeding the limits of the board.
	 *
	 * @param tab      Refers to the board.
	 * @param posInit  Refers to the initial position.
	 * @param posFinal Refers to the final position.
	 * @returns true if the path is clear until from init to final.
	 */
	public static boolean isValidPath(char[][] tab, int[] posInit,
	                                  int[] posFinal) {
		boolean flag = true;
		int[] variationDelta = moveDirection(posInit, posFinal);
		int deltaLines = posInit[0] + variationDelta[0];
		int deltaCol = posInit[1] + variationDelta[1];
		int[] pos = new int[]{deltaLines, deltaCol};
		if (posInit != posFinal) {
			while ((flag && deltaLines != posFinal[0]) ||
					(flag && deltaCol != posFinal[1])) {
				flag = isEmpty(tab, pos) || canLand(tab, posInit, pos);
				deltaLines += variationDelta[0];
				deltaCol += variationDelta[1];
				pos[0] = deltaLines;
				pos[1] = deltaCol;
			}
		}
		return flag;
	}

	/**
	 * Secondary method to check whether the move is horizontal, vertical or
	 * oblique. It returns a vector accordingly.
	 *
	 * @param posInit  refers to the initial position.
	 * @param posFinal refers to the final position.
	 * @returns a vector that indicates whether the direction is Horizontal
	 * {0, +/-1}, Vertical {+/-1, 0} or Oblique {+/-1, +/- 1}.
	 */
	public static int[] moveDirection(int[] posInit, int[] posFinal) {
		int[] vec = new int[]{0, 0};
		for (int i = 0; i < 2; i++) {
			if (posInit[i] > posFinal[i]) {
				vec[i] = -1;
			} else if (posInit[i] < posFinal[i]) {
				vec[i] = 1;
			}
		}
		return vec;
	}

	/**
	 * Secondary method to evaluate if given position is empty.
	 *
	 * @param tab refers to the board
	 * @param pos refers to the position itself
	 * @returns true if empty, false otherwise
	 */
	public static boolean isEmpty(char[][] tab, int[] pos) {
		return tab[pos[0]][pos[1]] == 'v';
	}

	/**
	 * Method to check if the landing position is of the opposite color in case
	 * it is not empty ('v').
	 *
	 * @param tab refers to the board
	 * @param pos refers to the current position
	 * @returns true or false accordingly.
	 */
	public static boolean canLand(char[][] tab, int[] initPos, int[] pos) {
		boolean canLand = false;
		char initColor = tab[initPos[0]][initPos[1]];
		char landingColor = tab[pos[0]][pos[1]];
		if (Character.isLowerCase(landingColor) && !isEmpty(tab, pos)) {
			if (Character.isUpperCase(initColor)) {
				canLand = true;
			}
		} else if (Character.isLowerCase(initColor) && !isEmpty(tab, pos)) {
			if (Character.isUpperCase(landingColor)) {
				canLand = true;
			}
		} else if (Character.isLowerCase(landingColor) && isEmpty(tab, pos)) {
			canLand = true;
		}
		return canLand;
	}

};
