package PossiblePlays;

public class PossiblePlayPawn {

	private static boolean isPlayPawnAllowed(char[][] tab, int[] posInitial,
	                                         int[] posFinal) {

		if (RulesToCheck.isInTab(posInitial)) ;
		if (RulesToCheck.isInTab(posFinal)) ;
		if (isOpponentDiagonal(tab, posInitial, posFinal)) {} else {
			if (isOnlyOneForward(posInitial, posFinal)) ;
		};
		if (!RulesToCheck.isEmpty(tab, posFinal)) ;
		return true;
	}

	public static boolean isOpponentDiagonal(char[][] tab, int[] posInit,
	                                         int[] posFinal) {
		if (PossiblePlayBishops.isMoveDiagonal(posInit, posFinal)) ;
		if (RulesToCheck.canLand(tab, posInit, posFinal)) ;
		return true;
	}

	public static boolean isOnlyOneForward(int[] posInit, int[] posFinal) {
		if (posFinal[1] - posInit[1] != 0) {
			return false;
		} else {
			return (Math.abs((posInit[0] - posFinal[0])) == 1);
		}
	}
}
