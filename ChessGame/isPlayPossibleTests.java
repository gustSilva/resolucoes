package PossiblePlaysTests;

import PossiblePlays.isPlayPossible;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class isPlayPossibleTests {

	@Test
	public void isValidForTower() {
		char[][] tab = {
				{'t', 'v', 'v', 'v', 'v', 'P', 'T'},
				{'b', 'p', 'v', 'v', 'v', 'P', 'B'},
				{'c', 'p', 'v', 'v', 'v', 'P', 'C'},
				{'k', 'p', 'v', 'v', 'v', 'P', 'K'},
				{'q', 'p', 'v', 'v', 'v', 'P', 'Q'},
				{'c', 'p', 'v', 'v', 'v', 'P', 'C'},
				{'b', 'p', 'v', 'v', 'v', 'P', 'B'},
				{'t', 'p', 'v', 'v', 'v', 'P', 'T'},
		};
		char piece = 't';
		int[] posInit = {0,1};
		int[] posFinal = {0, 3};
		assertTrue("Shows it will switch the board looks to the new " +
				"positions requested by the user",
				isPlayPossible.isPlayPossible(tab, posInit, posFinal, piece));
	}

	@Test
	public void isValidForTowerTrue() {
		char[][] tab = {
				{'v', 'v', 'v', 'v', 'v', 'P', 'T'},
				{'b', 'v', 'v', 'v', 'v', 'P', 'B'},
				{'c', 'p', 't', 'P', 'v', 'P', 'C'},
				{'k', 'p', 'v', 'v', 'v', 'P', 'K'},
				{'q', 'p', 'v', 'v', 'v', 'P', 'Q'},
				{'c', 'p', 'v', 'v', 'v', 'P', 'C'},
				{'b', 'p', 'v', 'v', 'v', 'P', 'B'},
				{'t', 'p', 'v', 'v', 'v', 'P', 'T'},
		};
		char piece = 't';
		int[] posInit = {2,2};
		int[] posFinal = {3, 2};
		assertTrue("Shows it will switch the board looks to the new " +
				"positions requested by the user",
				isPlayPossible.isPlayPossible(tab, posInit, posFinal, piece));
	}
}
