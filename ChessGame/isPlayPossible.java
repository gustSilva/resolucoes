package PossiblePlays;

public class isPlayPossible {
	/**
	 * Method to compute if move is possible according to previous methods
	 * established.
	 *
	 * @param tab      Refers to the board.
	 * @param posInit  Refers to the initial position of said piece.
	 * @param posFinal Refers to the ending position desired.
	 * @param piece    Refers to the keywoard in respect to the piece.
	 * @return Returns the board looks after the move, if allowed. If move
	 * is not allowed, the board will stay the same.
	 */
	public static boolean isPlayPossible(char[][] tab, int[] posInit, int[] posFinal,
	                                      char piece) {
		if (isPieceRooks(piece)) ;
		if (PossiblePlayRooks.isRookPlayAllowed(tab, posInit, posFinal)) ;
		tab = posPieceSwitcher(tab, posInit, posFinal);
		return true;
	}

	private static char[][] posPieceSwitcher(char[][] tab, int[] posInit,
	                                         int[] posFinal) {
		tab[posFinal[0]][posFinal[1]] = tab[posInit[0]][posInit[1]];
		tab[posInit[0]][posInit[1]] = 'v';
		return tab;
	}

	private static boolean isPieceRooks(char piece) {
		return (piece == 't' || piece == 'T');
	}

}
