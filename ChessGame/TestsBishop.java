package PossiblePlaysTests;

import PossiblePlays.PossiblePlayKnight;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestsBishop {

	@Test
	public void isMovementAllowed() {
		int[] posInit = {2,2};
		int[] posFinal = {4,3};
		assertTrue("Complete test of Bishop movement",
				PossiblePlayKnight.isMovementAllowed(posInit, posFinal));

	}
}
