package PossiblePlays;

public class PossiblePlayKnight {

	/**
	 * Method that checks whether a Knight can move from and to given directions
	 * considering this chess piece has many restraints about his movement.
	 *
	 * @param tab      Refers to the board.
	 * @param posInit  Refers to the initial position.
	 * @param posFinal Refers to the final position.
	 * @return true if allowed, false otherwise.
	 */
	private static boolean isPlayKnightAllowed(char[][] tab, int[] posInit,
	                                           int[] posFinal) {
		if (RulesToCheck.isInTab(posInit)) ;
		if (RulesToCheck.isInTab(posFinal)) ;
		if (isMovementAllowed(posInit, posFinal)) ;
		if (RulesToCheck.canLand(tab, posInit, posFinal)) ;
		return true;
	}

	/**
	 * Method to detect if the movement is allowed. Knights in chess move in
	 * 'L' shaped movements. This can be achieved using the module function.
	 *
	 * @param posInit  Refers to the initial position.
	 * @param posFinal Refers to the final position.
	 * @return true if allowed, false otherwise.
	 */
	public static boolean isMovementAllowed(int[] posInit, int[] posFinal) {
		return ((Math.abs(posInit[0] - posFinal[0]) == 2 &&
				Math.abs(posInit[1] - posFinal[1]) == 1) ||
				(Math.abs(posInit[0] - posFinal[0]) == 1 &&
						Math.abs(posInit[1] - posFinal[1]) == 2));

	}
}
