# Nota importante

## Propósito ##

Repositório Git para ficheiros de resolução dd exercícios do SWiTCH.

### Contribuir ###

Adicionem livremente comentários e PRs se acharem outras maneiras de resolver os exercícios.

## Contacto ##
gsilvapt (twitter, github) ou pelo n.º de aluno 1171643.