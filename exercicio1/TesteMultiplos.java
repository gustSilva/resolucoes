package tests;

import exAlgoritmos.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class TesteMultiplos {

	@Test
	public void test() {
		/* by: gsilvapt
		 * 
		 */
		assertEquals(Multiplos.multiplosTresECinco(1, 15), Arrays.asList(15)); // Valida que numa lista de 1 a 15, o resultado é 15
		assertEquals(Multiplos.multiplosTresECinco(1, 14), Arrays.asList()); // Valida que é gerado uma lista vazia
		assertEquals(Multiplos.multiplosTresECinco(1, 30), Arrays.asList(30, 15)); // Valida o resultado
		assertEquals(Multiplos.multiplos(3, 1, 6), Arrays.asList(3, 6)); // Confirma que os multiplos de 3, de 1 a 6, é apenas 6
		assertEquals(Multiplos.somaMultiplos(3, 1, 10), 18); // Soma os múltiplos de 3, no intervalo de 1 a 10

	}

}
