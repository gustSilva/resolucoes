package exAlgoritmos;

import java.util.*;

public class Multiplos {
	/* by: gsilvapt
	 * Funções para calcular múltiplos, em intervalos dados.
	 * A última função soma os múltiplos, de um dado número, de um dado intervalo.
	 */
	public static List<Integer> multiplosTresECinco(int a, int b) {
		List<Integer> multiplosTresECinco = new ArrayList<Integer>(); 
		for(int i = a; i <= b; i++) {
			if(i % 3 == 0 && i % 5 == 0) {
				multiplosTresECinco.add(i);
			}
		}
			Collections.reverse(multiplosTresECinco);
		return multiplosTresECinco;
	}

	public static List<Integer> multiplos(int m, int a, int b) {
		List<Integer> multiplos = new ArrayList<Integer>(); 
		for(int i = a; i <= b; i++) {
			if(i % m == 0) {
				multiplos.add(i);
			}
		}
		return multiplos;
	}

	public static int somaMultiplos(int m, int a, int b) {
		List<Integer> multiplosASomar = Multiplos.multiplos(m, a, b);
		int multiplosSomados = 0;
		for (int index=0; index < multiplosASomar.size(); index++) {
			multiplosSomados += multiplosASomar.get(index);
		}
		return multiplosSomados;
	}
}