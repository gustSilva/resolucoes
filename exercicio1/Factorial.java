package maxmin;
import java.lang.ArithmeticException;

public class Factorial {
	/* by: gsilvapt
	 * Função para calcular o fatorial de um número dado.
	 * Acrescenta excepção se o número dado for negativo.
	 */
	public static long Fact(long n) {
		long FACT = 1;
		if (n >= 0) {
			for(long i = 2; i <= n; i++) {
			FACT = FACT * i;
			}
		} else {
			throw new ArithmeticException("Factorial Inválido.");
		}
		return FACT;
	}

}
