package exAlgoritmos;

public class SomaPares {
	/* by: gsilvapt
	 * Função para retornar a somar de todos os números pares num intervalo de a a b.
	 */
	public static long SomaPares(int a, int b) {
		int somaPares = 0;
		for(int i = a; i <= b; i++) {
			if(i % 2 == 0) {
				somaPares += i;
			}
		}	
		return somaPares;
	}
}
