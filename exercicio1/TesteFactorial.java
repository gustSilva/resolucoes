package tests;

import maxmin.Factorial;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;

public class TesteFactorial {
	/* by: gsilvapt
	 * Testes para validar os resultados de fatorial.
	 * Falta verificar se a excepção é enviada, no caso de fatores negativos.
	 */

	@Test
	public void test() {
		assertEquals(Factorial.Fact(5), 120);
		assertEquals(Factorial.Fact(0), 1);
	}
	@Test(expected = ArithmeticException.class)
	public void exceptionTest(){
		assertEquals(Factorial.Fact(-2), 0);
	}
}
