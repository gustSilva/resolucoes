package tests;

import maxmin.Fibonaccci;
import static org.junit.Assert.*;

import org.junit.Test;

public class TesteFibonacci {

	@Test
	public void test() {
		assertEquals(Fibonaccci.Fibonacci(3), 2);
		assertEquals(Fibonaccci.Fibonacci(12), 144);
		assertEquals(Fibonaccci.Fibonacci(20), 6765);
	}

}
