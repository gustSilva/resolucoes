package tests;

import static org.junit.Assert.*;
import maxmin.Capicua;
import org.junit.Test;

public class TesteCapicua {
	/* by: gsilvapt
	 * Testes para validar os resultados da função que verifica se o número é capicua. 
	 * O caso específico de 10001 e -10001 são acrescentados propositadamente.
	 */
	@Test
	public void test() {
		assertEquals(Capicua.Capicua(22), true);
		assertEquals(Capicua.Capicua(12), false);
		assertEquals(Capicua.Capicua(1221), true);
		assertEquals(Capicua.Capicua(16513), false);
		assertEquals(Capicua.Capicua(10001), true);
		assertEquals(Capicua.Capicua(-10001), true);

	}

}
