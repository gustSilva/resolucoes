package maxmin;

public class Capicua {
	/* by: gsilvapt
	 * Função que valida se o número dado é uma capicua.
	 * Retorna true ou falso.
	 */
	public static boolean Capicua(long num) {
		long backup = num;
		long inverso = 0;
		while (num != 0) {
			inverso = (inverso * 10) + (num % 10);
			num = num/10;
		}
		if (inverso == backup) {
			return true;
		} else {
			return false;
		}
	}
}
