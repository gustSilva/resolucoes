package exercicios;

public class IsMatrizTriangular {
	public static boolean isMatrixTriang(int[][] matrix) {
		if (!isSquared(matrix)) {
			return false;
		} ;
		if (!isMatrixTriangSUP(matrix) || !isMatrixTriangINF(matrix)) {
			return false;
		};
		return true;
	}

	private static boolean isSquared(int[][] matrix) {
		int lineLength = matrix.length;
		int colLength = matrix[0].length;
		if (lineLength == colLength) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isMatrixTriangSUP(int[][] matrix) {
		boolean isTriangSup = true;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = i+1; j < matrix.length; j++) {
				if (matrix[j][i] != 0) {
					return isTriangSup = false;
				} else {
					isTriangSup = true;
				}
			}
		}
		return isTriangSup;
	}

	public static boolean isMatrixTriangINF(int[][] matrix) {
		boolean isTriangInf = true;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = i+1; j < matrix.length; j++) {
				if (matrix[j][i] != 0) {
					return isTriangInf = false;
				} else {
					isTriangInf = true;
				}
			}
		}
		return isTriangInf;
	}
}
