package exercicios;

public class NormalizaVector {

	public static double[] Normalizer(int[] vec) {
		if (!isNormalizable(vec)) {
			return new double[0];
		}
		double[] auxVec = new double[vec.length];
		for (int i = 0; i < vec.length; i++) {
			auxVec[i] = (vec[i] - Min(vec))/(Max(vec) - Min(vec));
		}
		return auxVec;
	}

	public static float Min(int[] vec) {
		int minAux = vec[0];
		for (int i = 1; i < vec.length; i++) {
			if(minAux > vec[i]) {
				minAux = vec[i];
			}
		}
		return minAux;
	}

	private static double Max(int[] vec) {
		int maxAux = vec[0];
		for (int i = 0; i < vec.length; i++) {
			if (maxAux < vec[i] ) {
				maxAux = vec[i];
			}
		}
		return maxAux;
	}

	private static boolean isNormalizable(int[] vec) {
		return vec.length != 0;
	}

}
