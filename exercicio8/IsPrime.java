package exercicios;

public class IsPrime {
	public static boolean isPrime(int num) {
		boolean flag = true;
		int i = 2;
		while (i <= Math.sqrt(num)) {
			if (num % i == 0) {
				flag = false;
				i++;
			} else {
				flag = true;
				i++;
			}
		}
		return flag;
	}
}
