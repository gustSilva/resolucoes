package testes;

import exercicios.IsMatrizTriangular;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestesMatrizTriangular {

	@Test
	public void MatrizTriangTrue() {
		int[][] matrix = {
				{2, 3, 3},
				{0, 1, 3},
				{0, 0, 9},
		};
		assertTrue(IsMatrizTriangular.isMatrixTriang(matrix));
	}

	@Test
	public void MatrizTriangInfTrue() {
		int[][] matrix = {
				{0, 3, 3},
				{0, 1, 3},
				{0, 0, 0},
		};
		assertTrue(IsMatrizTriangular.isMatrixTriangINF(matrix));
	}

	@Test
	public void MatrizTriangSupFalse() {
		int[][] matrix = {
				{0, 0, 0},
				{2, 1, 0},
				{3, 4, 5},
		};
		assertFalse(IsMatrizTriangular.isMatrixTriangSUP(matrix));
	}

	@Test
	public void MatrizTriangSupFalse2() {
		int[][] matrix = {
				{0, 4, 0},
				{2, 0, 6},
				{3, 4, 0},
		};
		assertFalse(IsMatrizTriangular.isMatrixTriangSUP(matrix));
	}
		@Test
	public void MatrizTriangInfFalse() {
		int[][] matrix = {
				{1, 2, 3},
				{0, 0, 3},
				{9, 0, 0},
		};
		assertFalse(IsMatrizTriangular.isMatrixTriangSUP(matrix));
	}
}
