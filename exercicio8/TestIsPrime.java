package testes;

import exercicios.IsPrime;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestIsPrime {
	@Test
	public void isPrimeTrue(){
		assertTrue(IsPrime.isPrime(3));
	}

	@Test
	public void isPrimeFalse() {
		assertFalse(IsPrime.isPrime(25));
	}

	@Test
	public void isPrimeTrue2() {
		assertTrue(IsPrime.isPrime(11));
		assertTrue(IsPrime.isPrime(127));
	}
	@Test
	public void isPrimeFalse2() {
		assertFalse(IsPrime.isPrime(144));
	}
}
