package testes;

import exercicios.NormalizaVector;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TesteNormalizaVector {

	@Test
	public void NormalizadorTrue() {
		int[] vec = {1, 50, 100, 0};
		double[] result = {0.01, 0.5, 1, 0};
		assertArrayEquals(NormalizaVector.Normalizer(vec), result, 1);
	}

	@Test
	public void NormalizadorFalse() {
		int[] vec = {5};
		double[] result = {Double.NaN};
		assertArrayEquals(NormalizaVector.Normalizer(vec), result, 1);
	}
}
