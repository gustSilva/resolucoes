package exercicios;

public class SomaAlgarismos {
	public static int somaAlgarismo(int num) {
		int aux = 0;
		int soma = 0;
		int temp = num;
		while(num != 0){
			aux = num % 10;
			soma += aux;
			num /= 10;
			aux *= 10;
		}
		return soma;
	}
}
