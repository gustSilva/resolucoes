package Tests;

import SomaMatrizes.SomaMatrizes;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TesteSomaMatrizes {
	@Test
	public void SomaMatrizesTrue() {
		int[][] a  = new int[][]{{1, 2, 3}, {4, 3, 2}};
		int[] res = {6, 9};
		assertEquals("Uma matriz [1, 2, 3] [4, 3, 2] = [6] [9]", SomaMatrizes.somaMatrizes(a), res);
	}
	@Test
	public void SomaMatrizesTrue2() {
		int[][] a  = new int[][]{{1, 2, 3}};
		int[] res = {6};
		assertEquals("Uma matriz [1, 2, 3] = [6]", SomaMatrizes.somaMatrizes(a), res);
	}

	@Test
	public void somaMatrizesFalse() {
		int[][] a = new int[][]{};
		assertEquals("A soma de uma matriz vazia = 0", SomaMatrizes.somaMatrizes(a), 0);

	}

	@Test
	public void encontrarVetorMatriz() {
		int[][] a = new int[][]{{1, 2, 3}, {4, 5, 6}, {0, 12, 1}};
		int[] result = {1, 2, 3};
		assertEquals("Encontrar [1, 2, 3] numa matriz", SomaMatrizes.encontraVetor(a, result), true);
	}

	@Test
	public void encontrarVetorElementosDiff() {
		int[][] a = new int[][]{{1, 2, 3}, {4, 5, 6}, {0, 12, 1}};
		int[] result = {0, 2, 6};
		assertEquals("Encontrar os elementos [0, 2, 6] numa matrix", SomaMatrizes.encontraVetor(a, result), true);
	}

	@Test
	public void enconntrarVetorFalse() {
		int[][] a = new int[][]{{2, 1, 1}, {4, 5, 6}};
		int[] result = {1};
		assertEquals("Encontrar os elementos [1, 20, 3] numa matrix", SomaMatrizes.encontraVetor(a, result), true);
	}

}
