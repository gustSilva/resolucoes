package Tests;

import SomaMatrizes.SolucionadorSudoku;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestesSudoku {
	@Test
	public void SolucaoSudokuColunaTrue(){
		int[][] sudoku = {
				{7, 3, 4, 1, 5, 9, 8, 2, 6},
				{8, 2, 9, 3, 7, 6, 4, 5, 1},
				{1, 6, 5, 8, 2, 4, 9, 7, 3},
				{5, 7, 6, 9, 8, 3, 2, 1, 4},
				{3, 9, 1, 7, 4, 2, 5, 6, 8},
				{2, 4, 8, 5, 6, 1, 7, 3, 9},
				{4, 1, 2, 6, 9, 5, 3, 8, 7},
				{6, 5, 7, 4, 3, 8, 1, 9, 2},
				{9, 8, 3, 2, 1, 7, 6, 4, 5},
		};
		assertEquals("Confirma valores não repetidos nas colunas. (Expect True)",
				SolucionadorSudoku.AreColunasCorretas(sudoku), true);
	}
	@Test
	public void SolucaoSudokuColunaFalse(){
		int[][] sudoku = {
				{7, 2, 4, 1, 5, 9, 8, 2, 6},
				{8, 3, 9, 3, 7, 6, 4, 5, 1},
				{1, 6, 5, 8, 2, 4, 9, 7, 3},
				{5, 7, 6, 9, 8, 3, 2, 1, 4},
				{3, 9, 1, 7, 4, 2, 5, 6, 8},
				{2, 1, 2, 5, 2, 1, 2, 3, 2},
				{2, 1, 2, 6, 2, 5, 2, 8, 2},
				{6, 5, 7, 4, 3, 8, 1, 9, 2},
				{9, 3, 8, 2, 1, 7, 6, 4, 5},
		};
		assertEquals("Valida repetição valores colunas. Teste Incorrecto (Expect False)",
				SolucionadorSudoku.AreColunasCorretas(sudoku), false);
	}
	@Test
	public void SolucaoSudokuLinhaTrue(){
		int[] sudoku = {7, 3, 4, 1, 5, 9, 8, 2, 6};
		assertEquals("Confirma teste por linha. Elementos não repetidos (Expect True)",
				SolucionadorSudoku.isLinhaCorreta(sudoku), true);
	}
	@Test
	public void TransposeColunaTrue(){
		int[][] col = {{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}};
		int[] vec = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		assertArrayEquals("Valida que a col = vector {1...9} (Expect True)",
			SolucionadorSudoku.transpoeLinha(col, 0), vec);
	}
	@Test
	public void SolucaoSudokuLinhaFalse(){
		int[] vec = {7, 2, 7, 1, 5, 9, 8, 2, 6};
		assertEquals("Confirma teste por linha. Elementos repetidos (Expect False)",
				SolucionadorSudoku.isLinhaCorreta(vec), false);
	}
	@Test
	public void SudokuLinhasComNumerosMaioresQue9() {
		int [] vec = {7, 3, 14, 1, 5, 9, 8, 2, 6};
		assertEquals("Testa a hipótese de passar um array com valores superiores a 9 (False)",
		SolucionadorSudoku.isLinhaCorreta(vec), false);
	}

	@Test
	public void IsSudokuSolutionCorrect() {
		int [][] sudoku = {
			{7, 3, 4, 1, 5, 9, 8, 2, 6},
			{8, 2, 9, 3, 7, 6, 4, 5, 1},
			{1, 6, 5, 8, 2, 4, 9, 7, 3},
			{5, 7, 6, 9, 8, 3, 2, 1, 4},
			{3, 9, 1, 7, 4, 2, 5, 6, 8},
			{2, 4, 8, 5, 6, 1, 7, 3, 9},
			{4, 1, 2, 6, 9, 5, 3, 8, 7},
			{6, 5, 7, 4, 3, 8, 1, 9, 2},
			{9, 8, 3, 2, 1, 7, 6, 4, 5},
		};
		assertEquals("Teste final. Valida solução do Sudoku. Solução Correta - TRUE",
				SolucionadorSudoku._IsSolutionGood(sudoku), true);
	}
	@Test
	public void IsSudokuSolutionCorrectFalseLinha() {
		int [][] sudoku = {
			{7, 2, 4, 1, 5, 9, 8, 2, 6},
			{8, 3, 9, 3, 7, 6, 4, 5, 1},
			{1, 6, 5, 8, 2, 4, 9, 7, 3},
			{5, 7, 6, 9, 8, 3, 2, 1, 4},
			{3, 9, 1, 7, 4, 2, 5, 6, 8},
			{2, 4, 8, 5, 6, 1, 7, 3, 9},
			{4, 1, 2, 6, 9, 5, 3, 8, 7},
			{6, 5, 7, 4, 3, 8, 1, 9, 2},
			{9, 8, 3, 2, 1, 7, 6, 4, 5},
		};
		assertEquals("Teste final. Valida solução do Sudoku. Solução Errada - FALSE",
				SolucionadorSudoku._IsSolutionGood(sudoku), false);
	}
	@Test
	public void TransformacaoQuadrante() {
		int[][] matrix = {
				{1,2,3,4,5,6,7,8,9},
				{9,1,2,3,4,5,6,7,8},
				{8,9,1,2,3,4,5,6,7},
				{7,8,9,1,2,3,4,5,6},
				{6,7,8,9,1,2,3,4,5},
				{5,6,7,8,9,1,2,3,4},
				{4,5,6,7,8,9,1,2,3},
				{3,4,5,6,7,8,9,1,2},
				{2,3,4,5,6,7,8,9,1}};
		int[] actual = {4, 3, 2, 5, 4, 3, 6, 5, 4};
		assertArrayEquals("Teste de conversão de quadrante para Linha." +
				"(Expect True)",
				SolucionadorSudoku.QuadrantToLine(matrix, 5), actual);
	}

	@Test
	public void isTransposta1by1Correct() {
		int[][] matrix = {
				{1},
		};
		int[] result = {1};
		assertArrayEquals("Testa transposta de matriz com 1 só elemento",
				SolucionadorSudoku.transpoeLinha(matrix, 0), result);
	}
	@Test
	public void IsSudokuSolutionCorrectFalseQuadrantes() {
		int [][] sudoku = {
				{1,2,3,4,5,6,7,8,9},
				{9,1,2,3,4,5,6,7,8},
				{8,9,1,2,3,4,5,6,7},
				{7,8,9,1,2,3,4,5,6},
				{6,7,8,9,1,2,3,4,5},
				{5,6,7,8,9,1,2,3,4},
				{4,5,6,7,8,9,1,2,3},
				{3,4,5,6,7,8,9,1,2},
				{2,3,4,5,6,7,8,9,1}};
		assertEquals("Teste final. Valida solução do Sudoku. Solução Errada - FALSE",
				SolucionadorSudoku._IsSolutionGood(sudoku), false);
	}

}
