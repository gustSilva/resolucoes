package SomaMatrizes;

public class SomaMatrizes {

	public static int[] somaMatrizes(int[][] vec) {
		int aux = 0;
		int[] result = new int[vec.length];
		for (int i = 0; i < vec.length; i++) {
			for (int j = 0; j < vec[i].length; j++) {
				aux += vec[i][j];
			}
			result[i] = aux;
			aux = 0;
		}
		return result;
	}

	public static boolean encontraVetor(int[][] matrix, int[] vec) {
		boolean flag = true;
		for (int i = 0; i <vec.length; i++) {
			flag = false;
			for (int j = 0; j < matrix.length && flag == false; j++) {
				for (int k = 0; k < matrix[j].length && flag == false; k++) {
					flag = vec[i] == matrix[j][k];
				}
			}
		}
		return flag;
	}
}
